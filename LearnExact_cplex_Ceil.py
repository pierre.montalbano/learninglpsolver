import copy
import sys
import cplex
import sys
import os
import cProfile
import math
import time

def Ceil(v):
    epsilon = 1e-8
    if (math.floor(v) + epsilon > v):
        return math.floor(v)
    else:
        return math.ceil(v)
def Floor(v):
    epsilon = 1e-8
    if (math.ceil(v) - epsilon < v):
        return math.ceil(v)
    else:
        return math.floor(v)
def Round(v):
    if(v<0):
        return Ceil(v)
    else:
        return Floor(v)

#Performs a basic branch and bound with bound propagation.
#lb is the current lb
#varorder is a list of decision variables
#branch are the decision made so far (only used to debug)
#varassigned are the variables assigned
#varassigned1 are the variables assigned to 1
#CurrDec is the current decision variable
#DecoCtr is a boolean operator returning if a constraint is deconnected
def BasicBB(lb,branch,varassigned,varassigned1,CurrDec,DecoCtr):
    global ub1,nbnode1
    if(nbnode1!=0):
        #Assign the decision variable. If x_{11}=1 then x_{10}=0.
        varassigned.append(CurrDec[0])
        varassigned.append(CurrDec[0][:-1]+str(1-int(CurrDec[0][-1])))
        if(CurrDec[1]==1):
            varassigned1.append(CurrDec[0])
        else:
            varassigned1.append(CurrDec[0][:-1] + str(1 - int(CurrDec[0][-1])))
        Queue=copy.copy(Neighbor[VarIdx[CurrDec[0]]])
        for ctr in Neighbor[VarIdx[CurrDec[0][:-1] + str(1 - int(CurrDec[0][-1]))]]:
            if(ctr not in Queue):
                Queue.append(ctr)

        #Update the objective function according to the reduced costs of the last iteration.
        for i in range(len(variable_names)):
            if(variable_names[i]!='t' and variable_names[i][0]!='s' and  constcost+obj[i]> ub1 and variable_names[i] not in varassigned):
                assert variable_names[i] not in varassigned1
                varassigned.append(variable_names[i])
                for ctr in Neighbor[i]:
                    if(ctr not in Queue):
                        Queue.append(ctr)

        for i in range(len(Neighbor)):
            assert len(Neighbor[i])>0
        while(len(Queue)>0):
            k=Queue[0]
            Queue.pop(0)
            if(k<len(DecoCtr)):
                if(DecoCtr[k]==0):
                    Assigned,deco,conf = BoundPropagation_eq_noreason(k, varassigned, varassigned1)
                    if(Assigned != []):
                        for varname,val in Assigned:
                            assert varname[0]!='s'
                            if(varname not in varassigned):
                                varassigned.append(varname)
                                for idx in Neighbor[VarIdx[varname]]:
                                    if(idx not in Queue):
                                        Queue.append(idx)
                                if(val==1):
                                    varassigned1.append(varname)
                    if(deco):
                        DecoCtr[k]=1
    for varname in varassigned:
        if(varname[0]=='d' and varname not in varassigned1):
            modelLP.variables.set_upper_bounds(VarIdx[varname],0)
        if(varname[0]=='d'):
            assert varname[:-1] + str(1 - int(varname[-1])) in varassigned
    modelLP.solve()
    """for var in variable_names:
        print(var)
        print(modelLP.variables.get_upper_bounds(var))
        print(modelLP.variables.get_lower_bounds(var))"""
    nbnode1+=1
    if(modelLP.solution.get_status()==1):
        newlb=modelLP.solution.get_objective_value()
        nextvar=""
        k=0
        while(nextvar=="" and k<len(DecVar)):
            varname=DecVar[k]
            value = modelLP.solution.get_values(VarIdx[varname])
            nearest_lower_integer = Floor(value)
            nearest_higher_integer = Ceil(value)
            absdist=min(abs(value - nearest_lower_integer),abs(value - nearest_higher_integer))
            if absdist > 1e-6:
                nextvar=varname
            k=k+1
        assert Ceil(newlb) >=Ceil(lb)
        for varname in varassigned:

            if(varname[0]=='d' and varname not in varassigned1):
                modelLP.variables.set_upper_bounds(VarIdx[varname],cplex.infinity)
        if(Ceil(newlb)<ub1):
            if(nextvar!=""):
                branch[nextvar]=1
                lb1=BasicBB(newlb,copy.copy(branch),copy.copy(varassigned),copy.copy(varassigned1),[nextvar,1],copy.copy(DecoCtr))
                lb2=ub1
                if(Ceil(newlb)<ub1):
                    branch[nextvar]=0
                    lb2=BasicBB(newlb,copy.copy(branch),copy.copy(varassigned),copy.copy(varassigned1),[nextvar,0],copy.copy(DecoCtr))
                return min(lb1,lb2)
            else:
                if(ub1>Ceil(newlb)):
                    ub1=Ceil(newlb)
                return ub1
        else:
            return newlb
    else:
        for varname in varassigned:
            if(varname[0]=='d' and varname not in varassigned1):
                modelLP.variables.set_upper_bounds(VarIdx[varname],cplex.infinity)
        return infinity

def MaximumRule(ctr1,ctr2,PBvar):
    ResMax={}
    NegVal=0
    ResMax["rhs"]=min(ctr1["rhs"],ctr2["rhs"])
    for key in ctr1:
        if(key!="rhs" and key!=PBvar[:-1]+str(1-int(PBvar[-1]))):
            if(key in ctr2 and key !=PBvar):
                ResMax[key]=max(ctr1[key],ctr2[key])
                if(ResMax[key]<0):
                    NegVal+=ResMax[key]
            else:
                ResMax[key]=max(ctr1[key],0)
    assert PBvar[:-1]+str(1-int(PBvar[-1])) not in ResMax
    for key in ctr2:
        if(key!="rhs" and key not in ResMax and key !=PBvar):
            ResMax[key]=max(ctr2[key],0)
    keytoremove=[]
    for key in ResMax:
        if(ResMax[key]<0+1e-7 and ResMax[key]>0-1e-7):
            keytoremove.append(key)
    for key in keytoremove:
        del ResMax[key]
    return ResMax


def BoundPropagation_eq_noreason(ctr,varassigned,varassigned1):
    var_idx=constraints[ctr][0]
    var_coeff=constraints[ctr][1]
    Assigned=[]
    rhs=modelLP.linear_constraints.get_rhs(ctr)
    maxelement=0
    MaxW=0
    MinW=0
    activevar=[]
    Negslackvar=False
    for i  in range(len(var_idx)):
        elm=variable_names[var_idx[i]]
        if(elm[0]!='s'):
            if(elm not in varassigned):
                activevar.append((elm,var_coeff[i]))
                if(maxelement<var_coeff[i]):
                    maxelement=var_coeff[i]
                if(var_coeff[i]>0):
                    MaxW+=var_coeff[i]
                else:
                    MinW+=var_coeff[i]
            else:
                if(elm in varassigned1):
                    rhs-=var_coeff[i]
        else:
            Negslackvar=True
    if(Ceil(MaxW)==Floor(MaxW)):
        MaxW=Ceil(MaxW)
    if(Ceil(MinW)==Floor(MinW)):
        MaxW=Ceil(MaxW)
    if(Ceil(rhs)==Floor(rhs)):
        rhs=Ceil(rhs)
    if(rhs==MaxW):
        for var,coeff in activevar:
            if(coeff>0):
                Assigned.append((var,1))
            else:
                Assigned.append((var,0))
        return Assigned,True,False
    if(rhs==MinW and not Negslackvar):
        for var,coeff in activevar:
            if(coeff>0):
                Assigned.append((var,0))
            else:
                Assigned.append((var,1))
        return Assigned,True,False
    if(MinW-1e-7>rhs and not Negslackvar):
        return Assigned,True,True
    if(MaxW+1e-7<rhs):
        return Assigned,True,True
    if(MaxW-maxelement+1e-7 < rhs):
        for var,coeff in activevar:
            if MaxW-coeff+1e-7<rhs:
                assert var not in varassigned
                Assigned.append((var,1))
                rhs-=coeff
                MaxW-=coeff
    return Assigned,False,False

def BoundPropagation_eq(ctr,varassigned,varassigned1,VarAssigned,TplReason):

    var_idx=constraints[ctr][0]
    var_coeff=constraints[ctr][1]
    Assigned=[]
    rhs=consRhs[ctr]
    maxelement=0
    MaxW=0
    MinW=0
    activevar=[]
    reasonMin=[]
    reasonMax=[]
    Negslackvar=False
    Posslackvar=False
    for i  in range(len(var_idx)):
        if(len(VarAssigned)<i+1):
            VarAssigned.append(-1)
        elm=variable_names[var_idx[i]]
        if(elm[0]=='s'):
            Negslackvar=True
            if(var_coeff[i]>0):
                Posslackvar=True
        if(VarAssigned[i]==-1):
            if(elm not in varassigned):
                activevar.append((elm,var_coeff[i]))
                if(maxelement<var_coeff[i]):
                    maxelement=var_coeff[i]
                if(var_coeff[i]>0):
                    MaxW+=var_coeff[i]
                else:
                    MinW+=var_coeff[i]
            else:
                VarAssigned[i]=0
                if(elm in varassigned1):
                    VarAssigned[i]=1
                    rhs-=var_coeff[i]
                    assert elm in TplReason
                    ok=False
                    Q=[elm]
                    while len(Q)>0:
                        elm2=Q[0]
                        Q.pop(0)
                        for var in TplReason[elm2]:
                            if(var not in TplReason):
                                if(var_coeff[i]>0):
                                    reasonMin.append([var,var_coeff[i]])
                                else:
                                    reasonMax.append([var,var_coeff[i]])
                            else:
                                Q.append(var)
                else:
                    if(var_coeff[i]<0):
                        reasonMin.append((elm,var_coeff[i]))
                    else:
                        reasonMax.append((elm,var_coeff[i]))
        elif(VarAssigned[i]==1):
            rhs-=var_coeff[i]
            assert elm in TplReason
            ok=False
            Q=[elm]
            while len(Q)>0:
                elm2=Q[0]
                Q.pop(0)
                for var in TplReason[elm2]:
                    if(var not in TplReason):
                        if(var_coeff[i]>0):
                            reasonMin.append([var,var_coeff[i]])
                        else:
                            reasonMax.append([var,var_coeff[i]])
                    else:
                        Q.append(var)
            assert variable_names[var_idx[i]] in varassigned1
        else:
            if(var_coeff[i]<0):
                reasonMin.append((elm,var_coeff[i]))
            else:
                reasonMax.append((elm,var_coeff[i]))
            assert variable_names[var_idx[i]] in varassigned
    if(Ceil(MaxW)==Floor(MaxW)):
        MaxW=Ceil(MaxW)
    if(Ceil(MinW)==Floor(MinW)):
        MaxW=Ceil(MaxW)
    if(Ceil(rhs)==Floor(rhs)):
        rhs=Ceil(rhs)
    if(rhs==MaxW and not Posslackvar):
        RealReasonMax=[]
        for var,coeff in reasonMax:
            RealReasonMax.append(var)
        for var,coeff in activevar:
            if(coeff>0):
                Assigned.append((var,1,RealReasonMax))
            else:
                Assigned.append((var,0,RealReasonMax))
        return Assigned,True,False,VarAssigned,RealReasonMax
    if(rhs==MinW and not Negslackvar):
        RealReasonMin=[]
        for var,coeff in reasonMin:
            RealReasonMin.append(var)
        for var,coeff in activevar:
            if(coeff>0):
                Assigned.append((var,0,RealReasonMin))
            else:
                Assigned.append((var,1,RealReasonMin))
        return Assigned,True,False,VarAssigned,RealReasonMin
    if(MaxW+1e-7<rhs and not Posslackvar):
        return Assigned,True,True,VarAssigned,reasonMax
    if(MinW-1e-7>rhs and not Negslackvar):
        RealReasonMin=[]
        for var,coeff in reasonMin:
            RealReasonMin.append(var)
        return Assigned,True,True,VarAssigned,reasonMin
    if(MaxW-maxelement+1e-7 < rhs and not Posslackvar):
        for var,coeff in activevar:
            RealReasonMax=[]
            if MaxW-coeff+1e-7<rhs:
                assert var not in varassigned
                for varmax,coeffmax in reasonMax:
                    RealReasonMax.append(varmax)
                Assigned.append((var,1,RealReasonMax))
                rhs-=coeff
                MaxW-=coeff
    return Assigned,False,False,VarAssigned,reasonMax
def have_common_keys(dict1, dict2):
    return set(dict1.keys()).intersection(dict2.keys())

def concatenate_lists(lists):
    concatenated_list = []
    for lst in lists:
        concatenated_list.extend(lst)
    return list(set(concatenated_list))


#Performs a basic branch and bound with bound propagation and learning.
#lb is the current lb
#varorder is a list of decision variables
#branch are the decision made so far (only used to debug)
#varassigned are the variables assigned
#varassigned1 are the variables assigned to 1
#varassigned0 are the variables assigned to 0
#CurrDec is the current decision variable
#DecoCtr is a boolean operator returning if a constraint is deconnected
#depth current depth (Only used to analyse the learned constraint)
#BPremove is a dictionary, it associates a constraint and a reason for each value removal made by BP.
#VarAssignedInCtr gives for each constraint the already assigned variables, used to speed up bound propagation.
#TupleReason is designed to capture why a tuple variable has been assigned to 1. It is useless for primal variables because they are booleans and it is easy to identify x_{10} and x_{11}.
def LearnBB(lb,branch,CurrDec,NewObjCoef,varassigned,varassigned1,varassigned0,DecoCtr,depth,BPremove,VarAssignedInCtr,TupleReason):
    global ub,nbnode,addctr,LastRhs,G,maxcc,num_constraints,constraint_name
    BPcurrent={}
    ConflictingCtr=-1
    #Update the stucture if some constraints has been learned.
    while(num_constraints!=len(DecoCtr)):
        DecoCtr.append(0)
        VarAssignedInCtr.append([])
    while(len(NewObjCoef)!=len(variable_names)):
        NewObjCoef.append(0)
    NCremoved={}
    Queue=[]
    if(nbnode!=0):
        #Assign decision variable. When x_{11}=1 then x_{10}=0.
        varassigned.append(CurrDec[0])
        varassigned.append(CurrDec[0][:-1]+str(1-int(CurrDec[0][-1])))
        if(CurrDec[1]==1):
            varassigned1.append(CurrDec[0])
            varassigned0.append(CurrDec[0][:-1] + str(1 - int(CurrDec[0][-1])))
            assert CurrDec[0][:-1] + str(1 - int(CurrDec[0][-1]))!='d'
            TupleReason[CurrDec[0]]=[CurrDec[0][:-1] + str(1 - int(CurrDec[0][-1]))]
        else:
            varassigned1.append(CurrDec[0][:-1] + str(1 - int(CurrDec[0][-1])))
            TupleReason[CurrDec[0][:-1] + str(1 - int(CurrDec[0][-1]))]=[CurrDec[0]]
            varassigned0.append(CurrDec[0])
        Queue=copy.copy(Neighbor[VarIdx[CurrDec[0]]])
        for ctr in Neighbor[VarIdx[CurrDec[0][:-1] + str(1 - int(CurrDec[0][-1]))]]:
            if(ctr not in Queue):
                Queue.append(ctr)

    #Update the objective function according to the reduced costs of the last iteration and perfoms NC.
    for i in range(len(variable_names)):
        if(variable_names[i]!='t' and lb+NewObjCoef[i] > ub and variable_names[i][0]!='s' and variable_names[i] not in varassigned0):
            NCremoved[variable_names[i]]=NewObjCoef[i]
            varassigned.append(variable_names[i])
            varassigned0.append(variable_names[i])
            for ctr in Neighbor[i]:
                if(ctr not in Queue):
                    Queue.append(ctr)
    modelLP.objective.set_linear(list(enumerate(NewObjCoef)))
    #Performs hand made domain conistency. And update the set of variables assigned ( varassigned, varassigned1,varassigned0) and the store a reason for the values removed by domain consistency.
    while(len(Queue)>0 and ConflictingCtr==-1):
        k=Queue[0]
        Queue.pop(0)
        if(k<len(DecoCtr)):
            if(DecoCtr[k]==0):
                Assigned,deco,conf,VarAssigned,reason = BoundPropagation_eq(k, varassigned, varassigned1,copy.copy(VarAssignedInCtr[k]),TupleReason)
                VarAssignedInCtr[k]=VarAssigned
                if(Assigned != []):
                    for varname,val,reasondel in Assigned:
                        if(varname[0]!='s'):
                            if(varname not in varassigned):
                                varassigned.append(varname)
                                for idx in Neighbor[VarIdx[varname]]:
                                    if(idx not in Queue):
                                        Queue.append(idx)
                                if(val==1):
                                    assert varname not in reasondel
                                    assert len(reasondel)>0
                                    varassigned1.append(varname)
                                    TupleReason[varname]=reasondel
                                else:
                                    assert varname not in reasondel
                                    varassigned0.append(varname)
                                    assert len(reasondel)>0
                                    BPremove[varname]=(k,reasondel)
                                    BPcurrent[varname]=(k,reasondel)
                if(deco):
                    DecoCtr[k]=1
                if(conf):
                    FarkDic={}
                    for var,coeff in reason:
                        assert var in varassigned
                        FarkDic[var]=coeff*ub
                    FarkDic["rhs"]=max(ub*consRhs[k],ub)
                    TempBPremove=copy.copy(BPremove)
                    ok=False
                    did=[]
                    while(not ok):
                        ok=True
                        for key in list(TempBPremove.keys()):
                            if(TempBPremove[key][0] not in did and key in FarkDic and FarkDic[key]!=0):
                                for var in BPremove[key][1]:
                                    FarkDic[var]=FarkDic["rhs"]
                                    if(var in BPremove):
                                        TempBPremove[var]=BPremove[var]
                                        ok=False
                                did.append(TempBPremove[key][0])
                            FarkDic[key]=0
                            del TempBPremove[key]
                    for key in NCremoved:
                        FarkDic[key]=NCremoved[key]
                        if(NCremoved[key]<FarkDic["rhs"]):
                            FarkDic["rhs"]=NCremoved[key]
                    for var in list(FarkDic.keys()):
                        if(var != "rhs" and NewObjCoef[VarIdx[var]]< FarkDic[var] and var not in varassigned0):
                            FarkDic[var]=NewObjCoef[VarIdx[var]]
                        if(FarkDic[var]<0+1e-7):
                            del FarkDic[var]
                    assert len(FarkDic)>0
                    assert "rhs" in FarkDic
                    return FarkDic,infinity
        else:
            assert 1==0
    for i in range(len(Neighbor)):
        assert len(Neighbor[i])>0
    #Set to 0 all the variables assigned to 0.
    for varname in variable_names:
        if(varname[0]!='s'):
            if(varname in varassigned0):
                if(varbound[VarIdx[varname]]!=0):
                    modelLP.variables.set_upper_bounds(VarIdx[varname],0)
                    varbound[VarIdx[varname]]=0
            elif(varbound[VarIdx[varname]]!=cplex.infinity):
                modelLP.variables.set_upper_bounds(VarIdx[varname],cplex.infinity)
                varbound[VarIdx[varname]]=cplex.infinity
    modelLP.solve()
    nbnode+=1
    cons=0

    if(modelLP.solution.get_status()==1):
        DualSol=[]
        assert ConflictingCtr==-1
        k=0
        newlb=modelLP.solution.get_objective_value()+lb

        #print(str(nbnode)+" "+str(newlb)+" "+str(ub)+" "+str(addctr)+" "+str(CurrDec))
        if(Floor(newlb)==Ceil(newlb)):
            newlb=Ceil(newlb)
        obj=copy.copy(NewObjCoef)
        mostfrac=1
        nextvar=""
        k=0
        while(nextvar=="" and k<len(DecVar)):
            varname=DecVar[k]
            value = modelLP.solution.get_values(VarIdx[varname])
            nearest_lower_integer = Floor(value)
            nearest_higher_integer = Ceil(value)
            absdist=min(abs(value - nearest_lower_integer),abs(value - nearest_higher_integer))
            if absdist > 1e-6:
                nextvar=varname
            k=k+1
        #Get the dual solution. And the reduced costs. 
        if(newlb>=lb+1e-7):
            DualSol=modelLP.solution.get_dual_values()
            for var in variable_names:
                if(var not in varassigned0):
                    obj[VarIdx[var]]=modelLP.solution.get_reduced_costs(VarIdx[var])
                assert obj[VarIdx[var]] >= 0-1e-5 or var in varassigned
        assert Ceil(newlb) >=Ceil(lb)
        if(Ceil(newlb)<ub):
            if(nextvar!=""):
                #branch left and right
                branch[nextvar]=1
                Farkctr1,lb1=LearnBB(copy.copy(newlb),copy.copy(branch),[nextvar,1],copy.copy(obj),copy.copy(varassigned),copy.copy(varassigned1),copy.copy(varassigned0),copy.copy(DecoCtr),depth+1,copy.copy(BPremove),copy.copy(VarAssignedInCtr),copy.copy(TupleReason))
                lb2=copy.copy(ub)
                Farkctr2={}
                if(Ceil(newlb)<ub):
                    branch[nextvar]=0
                    Farkctr2,lb2=LearnBB(copy.copy(newlb),copy.copy(branch),[nextvar,0],copy.copy(obj),copy.copy(varassigned),copy.copy(varassigned1),copy.copy(varassigned0),copy.copy(DecoCtr),depth+1,copy.copy(BPremove),copy.copy(VarAssignedInCtr),copy.copy(TupleReason))
                resMax={}
                MaxRule={}
                Name=nextvar
                #If the two children returned some constraints and the lb has been increased then apply the maximum rule. 
                if(len(Farkctr2)>0 and len(Farkctr1)>0 and (newlb!=min(lb1,lb2))):
                    ctridx1=-1
                    ctridx2=-1
                    j=0
                    if(Name in Farkctr2):
                        ctridx2=0
                    if(Name[:-1]+str(1-int(Name[-1])) in Farkctr1):
                        ctridx1=0
                    #If decision variable appears in both constraints then apply the maximum rule, otherwise select the constraint without the decision variable.   
                    if((ctridx2!=-1 and ctridx1!=-1) or lb1==infinity or lb2==infinity):
                        MaxRule=MaximumRule(Farkctr1,Farkctr2,Name)
                        resMax=MaxRule
                    elif(ctridx1!=-1 and ctridx2==-1):
                        resMax=Farkctr2
                        lb1=lb2
                    else:
                        resMax=Farkctr1
                    nonzero=0

                    #Learn the constraint. 
                    if(len(MaxRule)>1 and 1==0):
                        FarkCoef=[]
                        Indx=[]
                        size=modelLP.linear_constraints.get_num()
                        SlackPos=False
                        for var in MaxRule:
                            if(var[0]=='s' and MaxRule[var]>0+1e-5):
                                SlackPos=True
                        for i in range(len(variable_names)):
                            var = variable_names[i]
                            assert i==modelLP.variables.get_indices(var)
                            if(var in MaxRule):
                                #TO DEBUG
                                """if(var!= "rhs" and var[0]!='s' and obj[VarIdx[var]]+1e-5 < MaxRule[var] and (var not in varassigned or var in varassigned1)):
                                    print(var)
                                    print(MaxRule[var])
                                    print( obj[VarIdx[var]])
                                    print(VarIdx[var])
                                    assert 1==0"""
                                if(var[0]=='s'):
                                    if(MaxRule[var]>0):
                                        nonzero+=1
                                        Indx.append(i)
                                        FarkCoef.append(MaxRule[var])
                                else:
                                    #Weaken the negative coefficients and apply saturation
                                    val=max(0,min(MaxRule[var],MaxRule["rhs"]))
                                    if(val!=0 and not SlackPos):
                                        nonzero+=1
                                        #Neighbor[VarIdx[var]].append(size)
                                    if(val!=0):
                                        Indx.append(i)
                                        FarkCoef.append(val)
                        assert MaxRule["rhs"]<cplex.infinity
                        if(MaxRule["rhs"]<cplex.infinity):
                            addedctr["a"+str(addctr)]=[nonzero,MaxRule["rhs"],depth,0,0,0,0,0]
                            modelLP.variables.add(names=["s_"+str(addctr)], lb=[0.0])
                            variable_names.append("s_"+str(addctr))
                            assert len(variable_names)-1==modelLP.variables.get_indices("s_"+str(addctr))
                            VarIdx["s_"+str(addctr)]=len(variable_names)-1
                            FarkCoef.append(-1)
                            Neighbor.append([size])
                            Indx.append(len(variable_names)-1)
                            #assert  len(variable_names)-1==modelLP.variables.get_indices("s_"+str(addctr))
                            modelLP.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=Indx,val=FarkCoef)],senses=["E"],rhs=[MaxRule["rhs"]],range_values=[0.0],names=["add"+str(addctr)])
                            constraint_name.append("add"+str(addctr))
                            constraints.append([Indx,FarkCoef])
                            num_constraints+=1
                            addctr+=1
            else:
                if(ub>Ceil(newlb)):
                    ub=Ceil(newlb)
                lb1=newlb
                lb2=newlb
                resMax={}
        else:
            resMax={}
            lb1=newlb
            lb2=newlb

        FarkDic={}
        FarkDic["rhs"]=0
        if(newlb>=lb+1e-7):
            #TO DEBUG
            """for var in resMax:
                if(var!= "rhs" and var[0]!='s' and obj[VarIdx[var]]+1e-5 < resMax[var] and (var not in varassigned or var in varassigned1)):
                    print(var)
                    print(resMax[var])
                    print( obj[VarIdx[var]])
                    print(VarIdx[var])
                    assert 1==0"""
            considx=0
            #Construct the dual proof constraint.
            for cons in constraint_name:
                assert considx==modelLP.linear_constraints.get_indices(cons)
                if(cons[0]=='a'):
                    addedctr[cons][3]+=1
                if(len(DualSol)>considx and DualSol[considx]!=0):
                    if(cons[0]=='a'):
                        addedctr[cons][4]+=1
                    var_idx=constraints[considx][0]
                    var_coeff=constraints[considx][1]
                    for i in range(len(var_idx)):
                        elm=variable_names[var_idx[i]]
                        AddCost=DualSol[considx]*var_coeff[i]
                        if(elm in FarkDic):
                            FarkDic[elm]+=AddCost
                        else:
                            FarkDic[elm]=AddCost
                    FarkDic["rhs"]+=DualSol[considx]*consRhs[considx]
                considx+=1
        emptyfark=False
        if(len(FarkDic)==1):
            emptyfark=True
        for var in resMax:
            if(var in FarkDic):
                FarkDic[var]+=resMax[var]
            else:
                FarkDic[var]=resMax[var]

        #Replace the values removed by BP by its explanation.
        #TO DO: Test if we should replace only the values removed by domain consistency at the current node or all the values removed by BP until the beginning of the search
        if(FarkDic["rhs"]>0+1e-7):
            did=[]
            TempBPremove=copy.copy(BPremove)
            ok=False
            while(not ok):
                ok=True
                for key in list(TempBPremove.keys()):
                    if(TempBPremove[key][0] not in did and key in FarkDic and FarkDic[key]!=0):
                        for var in BPremove[key][1]:
                            FarkDic[var]=FarkDic["rhs"]
                            if(var in BPremove):
                                TempBPremove[var]=BPremove[var]
                                ok=False
                        did.append(TempBPremove[key][0])
                    FarkDic[key]=0
                    del TempBPremove[key]
            for key in NCremoved:
                FarkDic[key]=NCremoved[key]
                if(NCremoved[key]<FarkDic["rhs"]):
                    FarkDic["rhs"]=NCremoved[key]
            SlackPos=False
            if(len(NCremoved)>0):
                for var in list(FarkDic.keys()):
                    if(var!="rhs" and var[0]!='s' and FarkDic[var]<0):
                        del FarkDic[var]
            #Learn the constraint
            if(FarkDic["rhs"]>0+1e-7 and not emptyfark):
                FarkCoef=[]
                nonzero=0
                size=modelLP.linear_constraints.get_num()
                Indx=[]
                for var in FarkDic:
                    if(var[0]=='s'):
                        if(FarkDic[var]>0+1e-5):
                            SlackPos=True
                while(len(NewObjCoef)!=len(variable_names)):
                    NewObjCoef.append(0)
                #Construct the constraint
                for i in range(len(variable_names)):
                    var=variable_names[i]
                    assert modelLP.variables.get_indices(var)==i

                    if(var in FarkDic):
                        #TO DEBUG
                        """if(var!= "rhs" and NewObjCoef[VarIdx[var]]+1e-5 < FarkDic[var] and (var not in varassigned or var in varassigned1)):
                            #FarkDic[modif][var.name]=NewObjCoef[VarIdx[var.name]]
                            print(var)
                            print(FarkDic[var])
                            print( NewObjCoef[VarIdx[var]])
                            print( obj[VarIdx[var]])

                            print(VarIdx[var])
                            assert 1==0"""
                        if(FarkDic[var]>=0-1e-7 and FarkDic[var]<=0+1e-7):
                            del FarkDic[var]
                        else:
                            assert FarkDic[var]!=0
                            if(var[0]=='s'):
                                if(FarkDic[var]>0):
                                    nonzero+=1
                                    Indx.append(i)
                                    FarkCoef.append(FarkDic[var])
                            else:
                                #Weaken negative coefficients and saturate.
                                val=max(0,min(FarkDic[var],FarkDic["rhs"]))
                                if(val!=0 and not SlackPos):
                                    nonzero+=1
                                    #Neighbor[VarIdx[var]].append(size)
                                if(val!=0):
                                    Indx.append(i)
                                    FarkCoef.append(val)
                modelLP.variables.add(names=["s_"+str(addctr)], lb=[0.0])
                variable_names.append("s_"+str(addctr))
                assert len(variable_names)-1==modelLP.variables.get_indices("s_"+str(addctr))
                VarIdx["s_"+str(addctr)]=len(variable_names)-1
                FarkCoef.append(-1)
                Neighbor.append([size])

                Indx.append( len(variable_names)-1)
                #assert len(variable_names)-1==modelLP.variables.get_indices("s_"+str(addctr))
                modelLP.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=Indx,val=FarkCoef)],senses=["E"],rhs=[FarkDic["rhs"]],range_values=[0.0],names=["add"+str(addctr)])
                constraint_name.append("add"+str(addctr))
                constraints.append([Indx,FarkCoef])
                consRhs.append(modelLP.linear_constraints.get_rhs("add"+str(addctr)))
                addedctr["add"+str(addctr)]=[len(FarkCoef),modelLP.linear_constraints.get_rhs("add"+str(addctr)),depth,0,0,0,0,0]
                num_constraints+=1
                addctr+=1
                if(depth==0):
                    LastRhs=FarkDic["rhs"]
        return FarkDic,min(lb1,lb2)
    else:
        FarkDic={}
        DualFark = modelLP.solution.get_dual_values()
        k=0
        rhs=0
        #We obtain an unbounded dual ray, we multiply by 1000 the coefficients to be sure it will not decrease the coefficients when using the maximum rule.
        for considx in range(len(constraint_name)):
            if(DualFark[k]!=0):
                var_idx=constraints[considx][0]
                var_coeff=constraints[considx][1]
                rhs+=DualFark[k]*consRhs[considx]*1000
                for i in range(len(var_idx)):
                    elm=variable_names[var_idx[i]]
                    AddCost=DualFark[k]*var_coeff[i]*1000
                    if elm in FarkDic:
                        FarkDic[elm]+=AddCost
                    else:
                        FarkDic[elm]=AddCost
            k+=1
        FarkDic["rhs"]=rhs
        for var in list(FarkDic.keys()):
            if(FarkDic[var]<0+1e-7):
                del FarkDic[var]

        if(rhs>0+1e-7):
            did=[]
            while(len(NewObjCoef)!=len(variable_names)):
                NewObjCoef.append(0)
            TempBPremove=copy.copy(BPremove)
            ok=False
            while(not ok):
                ok=True
                for key in list(TempBPremove.keys()):
                    if(TempBPremove[key][0] not in did and key in FarkDic and FarkDic[key]!=0):
                        for var in BPremove[key][1]:
                            FarkDic[var]=FarkDic["rhs"]
                            if(var in BPremove):
                                TempBPremove[var]=BPremove[var]
                                ok=False
                        did.append(TempBPremove[key][0])
                    FarkDic[key]=0
                    del TempBPremove[key]
            for key in NCremoved:
                FarkDic[key]=NCremoved[key]
                if(NCremoved[key]<FarkDic["rhs"]):
                    FarkDic["rhs"]=NCremoved[key]
            for var in list(FarkDic.keys()):
                if(var != "rhs" and NewObjCoef[VarIdx[var]]< FarkDic[var] and var not in varassigned0):
                    FarkDic[var]=NewObjCoef[VarIdx[var]]
                if(FarkDic[var]<0+1e-7):
                    del FarkDic[var]
            assert len(FarkDic)>1
        else:
            FarkDic=[]
        return FarkDic,infinity

if(sys.argv[1][-2:]=="lp"):
    FileName=sys.argv[1]
elif(sys.argv[1][-2:]=="sp"):
    os.system("rm "+sys.argv[1][0:-5]+".lp")
    os.system("python2 wcsp2lp-support_learn.py "+ sys.argv[1]+" "+sys.argv[1][0:-5]+".lp >/dev/null")
    FileName=sys.argv[1][0:-5]+".lp"
elif(sys.argv[1][-2:]=="xz"):
    os.system("xz -d "+ sys.argv[1])
    os.system("python2 wcsp2lp-support_learn.py "+ sys.argv[1][:-3]+" "+sys.argv[1][0:-8]+".lp >/dev/null")
    FileName=sys.argv[1][0:-8]+".lp"
    os.system("xz -k9 "+ sys.argv[1][:-3])
modelLP = cplex.Cplex()
st=time.time()
modelLP.parameters.lpmethod.set(1)
modelLP.parameters.preprocessing.presolve.set(0) # Disable presolve completely
modelLP.parameters.preprocessing.reduce.set(0)   # Disable empty column removal
modelLP.parameters.preprocessing.aggregator.set(0) # Disable aggregator
modelLP.parameters.preprocessing.dual.set(0) # Disable dual reduction
modelLP.parameters.preprocessing.boundstrength.set(1) # Disable bound strengthening
# Enable bound tightening during presolve
modelLP.parameters.threads.set(1)
modelLP.set_log_stream(None)
modelLP.set_results_stream(None)

modelLP.read(FileName)
num_variables = modelLP.variables.get_num()
variable_names = modelLP.variables.get_names()

modelLP.set_problem_type(cplex.Cplex.problem_type.LP)
num_constraints = modelLP.linear_constraints.get_num()
DecVar=[]
i=0
VarIdx={}
k=0
LastRhs=0
addedctr={}
Neighbor=[]
varbound=[]
consRhs=[]
obj=modelLP.objective.get_linear()
for var in variable_names:
    if(var[0]=='d' and var[-1]=='0'):
        DecVar.append(var)
    if var !='t':
        modelLP.variables.set_upper_bounds(var,cplex.infinity)
    varbound.append(cplex.infinity)
    VarIdx[var] = modelLP.variables.get_indices(var)
    Neighbor.append([])
constraint_name = modelLP.linear_constraints.get_names()
constraints=[]

for cons in constraint_name:
    sparse=modelLP.linear_constraints.get_rows(cons)
    var_idx,var_coeff=sparse.unpack()
    constraints.append([var_idx,var_coeff])
    consRhs.append(modelLP.linear_constraints.get_rhs(cons))
j=0
for sparse in modelLP.linear_constraints.get_rows():
    ind,val=sparse.unpack()
    for var in ind:
        Neighbor[var].append(j)
    j=j+1
branch={}
infinity=100000
ub=10000000
nbnode=0
nbnode1=0

addctr=0
#SolveBB(model.getObjVal(),varorder,branch)

#quicksum(var_list[i]*FarkCoef[i] for i in range(var_list_len)
midtime = time.time()
maxcc=0

if(len(sys.argv)==3):
    DecVar.clear()
    with open(sys.argv[2], 'r') as file:
        # Read the single line from the file and remove trailing newline character
        line = file.readline().strip()
    for var in line.split():
        DecVar.insert(0,"d"+var+"_1")
#cProfile.run('LearnBB(0,branch,[],copy.copy(obj),[],[],[],[],0,{},[],{})')
LearnBB(0,branch,[],copy.copy(obj),[],[],[],[],0,{},[],{})
aftertime=time.time()
meanize=0
depthdata={}
for ctr in addedctr:
    if(addedctr[ctr][2] not in depthdata):
        depthdata[addedctr[ctr][2]]=[1,addedctr[ctr][0],addedctr[ctr][1],addedctr[ctr][3],addedctr[ctr][4]]
    else:
        depthdata[addedctr[ctr][2]][0]+=1
        depthdata[addedctr[ctr][2]][1]+=addedctr[ctr][0]
        depthdata[addedctr[ctr][2]][2]+=addedctr[ctr][1]
        depthdata[addedctr[ctr][2]][3]+=addedctr[ctr][3]
        depthdata[addedctr[ctr][2]][4]+=addedctr[ctr][4]
    meanize+=addedctr[ctr][0]
meanize=meanize/len(addedctr)
#SolveBB(model.getObjVal(),varorder,branch)

print("Learning optimum: "+str(ub)+" nodes: "+str(nbnode)+" time: "+str(aftertime-midtime)+" number of learned constraints: "+str(addctr)+" average size: " +str(meanize))
#for data in depthdata:
    #print("depth: "+str(data)+ " nb ctr "+str(depthdata[data][0])+" average size: "+str(depthdata[data][1]/depthdata[data][0])+" average rhs: "+str(depthdata[data][2]/depthdata[data][0])+" appearing: "+str(depthdata[data][3])+ " isuseful: "+str(depthdata[data][4]) + " rapport: "+str(depthdata[data][3]/max(depthdata[data][4],1)))
#[size ctr, rhs, depth,dual!=0,nb active, sum dual,sum dual*rhs]
nonzero=0
modelLP.parameters.preprocessing.presolve.set(1)
#modelLP.write("learn.lp")
#print("With learning, the optimal solution is "+str(ub)+" in "+str(nbnode)+" nodes and "+str(addctr)+" constraints learned")
modelLP.read(FileName)
modelLP.set_problem_type(cplex.Cplex.problem_type.LP)
constraint_name = modelLP.linear_constraints.get_names()

i=0
VarIdx={}
k=0
LastRhs=0
addedctr={}
Neighbor=[]
obj=modelLP.objective.get_linear()
variable_names = modelLP.variables.get_names()
constcost=0
for i in range(len(variable_names)):
    var=variable_names[i]
    if var !='t':
        modelLP.variables.set_upper_bounds(var,cplex.infinity)
    if(var=='t'):
        constcost=obj[i]
    VarIdx[var] = modelLP.variables.get_indices(var)
    Neighbor.append([])
j=0
constraints=[]
for cons in modelLP.linear_constraints.get_names():
    sparse=modelLP.linear_constraints.get_rows(cons)
    var_idx,var_coeff=sparse.unpack()
    constraints.append([var_idx,var_coeff])
for sparse in modelLP.linear_constraints.get_rows():
    ind,val=sparse.unpack()
    for var in ind:
        Neighbor[var].append(j)
    j=j+1
branch={}
infinity=100000
ub1=10000000
currnode1=0


AddedCtr=[]
beforebasic=time.time()
Dec=[0]*num_constraints
VarAssignedInCtr=[0]*num_constraints
#cProfile.run('BasicBB(0,branch,[],[],[],Dec)')
"""BasicBB(0,branch,[],[],[],Dec)
#LearnBB2(model1.getObjVal(),varorder1,{},[],[])
#modelLP.writeProblem("model2.lp")
afterbasic=time.time()
print("No learning optimum: "+str(ub1)+" nodes: "+str(nbnode1)+" time: "+str(afterbasic-beforebasic))
#cProfile.run('BasicBB(model1.getObjVal(),varorder1,branch,[])')
#print("Without learning, the optimal solution is "+str(ub1)+" in "+str(nbnode1)+" nodes ")

#print(str(ub)+" "+str(nbnode)+" "+str(aftertime-midtime)+" "+str(addctr)+" "+str(LastRhs)+" "+str(maxcc))"""

if(ub1!=ub):
    assert(1==0)
