//
// Created by root on 08/11/23.
//

#ifndef LEARNLPC___CONSTRAINT_H
#define LEARNLPC___CONSTRAINT_H
#include <iostream>
#include <ilcplex/ilocplex.h>
#include <ilconcert/ilomodel.h>
#include <ilcp/cp.h>
#include <vector>
#include <algorithm>
#include <cassert>
#include <unordered_map>
#include <cstddef>
#include <cstring>
#include <chrono>
using namespace std;
double Myceil(double v) {
    double epsilon = 1e-8;
    if (floor(v) + epsilon > v)
        return floor(v);
    else
        return ceil(v);
}
template <typename T>
std::ostream& operator<<(std::ostream& os, const std::vector<T>& vec) {
    os <<"("<<vec.size()<<") "<< "[ ";
    for (const T& element : vec) {
        os << element << " ";
    }
    os << "]";
    return os;
}

/* Performs domain consistency on a constraint
 * ctr is the constraint we want to propagate
 * varassigned gives the state of the variables
 * BPassign and BPremove are updated if domain consistency does something.
 *
 * */
tuple<bool,bool,vector<long>,vector<double>> BoundPropagation(IloRange ctr,vector<int> &varassigned,vector<vector<long>> &BPassign, vector<vector<long>> &BPremove) {
    auto it(ctr.getLinearIterator());
    vector<IloNumVar> var;
    vector<double> var_coeff;
    while (it.ok()) {
        var.push_back(it.getVar());
        var_coeff.push_back(it.getCoef());
        ++it;
    }
    double rhs = ctr.getUB();
    assert(ctr.getUB()==ctr.getLB());
    double maxelement = 0;
    double MaxW = 0,MinW = 0;
    long elm;
    vector<int> activevar;
    vector<long> reasonMin,reasonMax;
    vector<double> CoefMax, CoefMin;
    bool Negslackvar = false,Posslackvar = false;
    for (int i = 0; i < (int)var.size(); ++i) {
        if (var[i].getName()[0] == 's') {
            if(var_coeff[i] > 0)
                Posslackvar = true;
            else
                Negslackvar = true;
        }else if (varassigned[var[i].getId()-1] == -1) {
            activevar.push_back(i);
            if (maxelement < var_coeff[i])
                maxelement = var_coeff[i];
            if (var_coeff[i] > 0)
                MaxW += var_coeff[i];
            else
                MinW += var_coeff[i];

        }else if (varassigned[var[i].getId()-1] == 1) {
            rhs -= var_coeff[i];
            vector<long> Q;
            Q.push_back(var[i].getId()-1);
            while(!Q.empty() > 0 && !BPassign.empty())
            {
                elm = Q[0];
                Q.erase(Q.begin());
                for (int j = 0; j < (int)BPassign[elm].size(); ++j) {
                    if(BPassign[BPassign[elm][j]].empty()){
                        if (var_coeff[i] > 0) {
                            reasonMin.push_back(BPassign[elm][j]);
                            CoefMin.push_back(var_coeff[i]);
                        }
                        else {
                            reasonMax.push_back(BPassign[elm][j]);
                            CoefMax.push_back(var_coeff[i]);
                        }
                    }else
                        Q.push_back(BPassign[elm][j]);
                }
            }
        }else{
            assert(varassigned[var[i].getId()-1] == 0);
            if(var_coeff[i]<0) {
                reasonMin.push_back(var[i].getId() - 1);
                CoefMin.push_back(var_coeff[i]);
            }else {
                reasonMax.push_back(var[i].getId() - 1);
                CoefMax.push_back(var_coeff[i]);
            }
        }
    }
    if (Myceil(MaxW) == floor(MaxW))
        MaxW = Myceil(MaxW);
    if (Myceil(MinW) == floor(MinW))
        MinW = Myceil(MinW);
    if (Myceil(rhs) == floor(rhs))
        rhs = Myceil(rhs);
    if (rhs == MaxW and not Posslackvar) {
        vector<long> assigned;
        for (int i : activevar) {
            if(var_coeff[i]>0){
                if(!BPassign.empty())
                    BPassign[var[i].getId()-1]=reasonMax;
                varassigned[var[i].getId()-1]=1;
            } else{
                if(!BPremove.empty())
                    BPremove[var[i].getId()-1]=reasonMax;
                varassigned[var[i].getId()-1]=0;
            }
            assigned.push_back(var[i].getId()-1);
        }
        return {true, false,assigned,{}};
    }
    if (rhs == MinW and not Negslackvar) {
        vector<long> assigned;
        for (int i : activevar) {
            if(var_coeff[i]<0){
                if(!BPassign.empty())
                    BPassign[var[i].getId()-1]=reasonMin;
                varassigned[var[i].getId()-1]=1;
            } else{
                if(!BPremove.empty())
                    BPremove[var[i].getId()-1]=reasonMin;
                varassigned[var[i].getId()-1]=0;
            }
            assigned.push_back(var[i].getId()-1);
        }
        return {true, false,assigned,{}};
    }
    if (MaxW + 1e-7 < rhs and not Posslackvar) {
        return {true, true, reasonMax, CoefMax};
    }
    if (MinW - 1e-7 > rhs and not Negslackvar) {
        return {true, true, reasonMin, CoefMin};
    }
    if (MaxW - maxelement + 1e-7 < rhs and not Posslackvar) {
        vector<long> assigned;
        for (int i : activevar) {
            if (MaxW - var_coeff[i] + 1e-7 < rhs) {
                if(!BPassign.empty()) {
                    //assert(!reasonMax.empty());
                    BPassign[var[i].getId() - 1] = reasonMax;
                }
                varassigned[var[i].getId()-1] = 1;
                assigned.push_back(var[i].getId()-1);
            }
        }
        return {false,false,assigned,{}};
    }
    return {false,false,{},{}};
}
void Union(vector<long> *set1, vector<long> set){
    for (long i : set) {
        if(find(set1->begin(),set1->end(),i)==set1->end())
            set1->push_back(i);
    }
}
class MyConstraint {
public:
    vector<int> var;
    vector<double> coef;
    double rhs;
    MyConstraint(vector<int> var_,vector<double> coef_, double rhs_) {
        // Initialize the object's state based on the provided parameters
        this->var = var_;
        this->coef = coef_;
        this->rhs = rhs_;
    }
    MyConstraint(long size) {
        // Initialize the object's state based on the provided parameters
        for (int i = 0; i < size; ++i) {
            var.push_back(i);
            coef.push_back(0);
        }
        this->rhs = 0;
    }
    void BuildDualProof(IloNumArray duals, IloRangeArray ctr,unordered_map<string, long> SlackvariableMap) {
        assert(duals.getSize() == ctr.getSize());
        for (int i = 0; i < ctr.getSize(); ++i) {
            auto it(ctr[i].getLinearIterator());
            while (it.ok()) {
                if (it.getVar().getName()[0] != 's') {
                    assert(it.getVar().getId() - 1 < (int) coef.size());
                    coef[it.getVar().getId() - 1] += it.getCoef() * duals[i];
                } else {
                    assert(SlackvariableMap[it.getVar().getName()] < (int) coef.size());
                    coef[SlackvariableMap[it.getVar().getName()]] += it.getCoef() * duals[i];
                }
                ++it;
            }
            rhs += duals[i] * ctr[i].getUB();
        }
        if (rhs < 1e-7 && rhs > -1e-7) {
            rhs = 0;
            clear();
        }else{
            for (double &i: coef) {
                if (i < 1e-7 && i > -1e-7)
                    i = 0;
            }
        }

    }
    // Overload the >> operator as a friend function
    void display() const {
        cout<<coef <<" "<<rhs<<endl;
    }
    void clear() {
        rhs=0;
        coef.clear();
    }
    void CompleteCtr(long size){
        for (int i = (int) coef.size(); i < size; ++i) {
            coef.push_back(0);
        }
    }
    void BPFusion(vector<vector<long>> BPremove){
        bool ok=false;
        while(!ok) {
            ok= true;
            for (int i = 0; i < (int) BPremove.size(); ++i) {
                if (coef[i] != 0 && !BPremove[i].empty()) {
                    for (long j : BPremove[i]) {
                        coef[j] = rhs;
                    }
                    coef[i] = 0;
                    ok=false;
                }
            }
        }
    }
    void NCFusion(vector<double> NCremove){
        for (int i = 0; i < (int) NCremove.size(); ++i) {
            if(NCremove[i]!=-1) {
                coef[i] = NCremove[i];
                if (NCremove[i] < rhs)
                    rhs = NCremove[i];
            }
        }
    }
};

MyConstraint FusionResolution(MyConstraint ctr1, MyConstraint ctr2, long ResolveVar, vector<long> ResolveVarOpoosite){
    bool iszero;
    iszero=false;
    bool iszero2;
    iszero2=true;
    if(ctr2.coef[ResolveVar]==0)
        iszero=true;
    else
        ctr2.coef[ResolveVar]=0;
    for (int i : ResolveVarOpoosite) {
        if(ctr1.coef[i]!=0)
            iszero2=false;
        ctr1.coef[i]=0;
    }
    if(iszero && !iszero2){
        return ctr2;
    }else if (iszero2) {
        return ctr1;
    }
    assert(ctr1.coef.size()<= ctr2.coef.size());
    for (int i = 0; i < (int)ctr1.coef.size(); ++i) {
        ctr2.coef[i]= max(ctr1.coef[i],ctr2.coef[i]);
    }
    ctr2.rhs=min(ctr1.rhs,ctr2.rhs);

    return ctr2;
}
#endif //LEARNLPC___CONSTRAINT_H
