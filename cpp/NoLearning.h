//
// Created by root on 06/11/23.
//

#ifndef LEARNLPC___NOLEARNING_H
#define LEARNLPC___NOLEARNING_H
#include "constraint.h"

class BasicBB{
public:
    long ub;
    long nbvar;
    long nbconstr;
    long nbnodes;
    IloEnv env;
    IloModel model;
    int nbIteration;
    int nbCall;
    IloCplex cplex;
    IloNumVarArray variables;
    IloRangeArray constraints;
    vector<vector<long>> Neighbor; // Neighbor[i] gives the set of constraints involving variable i
    IloNumVarArray DecisionVariables;
    vector<vector<long>> OppositeDecisionVariables;
    int verbose;
    int nbslackvar;
    unordered_map<string, long> SlackvariableMap;
    double SecondSpendSolvingCPLEX;

    BasicBB(const char *file,int verbose_,string order):model(env), cplex(model), variables(env), constraints(env), DecisionVariables(env) {
        IloCP cp(model);
        cplex.setParam(IloCplex::Param::Preprocessing::Presolve, 0);
        cplex.setParam(IloCplex::Param::Threads, 1);
        cplex.setOut(env.getNullStream());  // Suppress log output
        cplex.setParam(IloCplex::IloCplex::Param::MIP::Tolerances::Integrality, 1e-9);  // Integer feasibility tolerance
        cplex.setParam(IloCplex::Param::Simplex::Tolerances::Optimality, 1e-9);  // Optimality tolerance
        cplex.setParam(IloCplex::Param::Feasopt::Tolerance, 1e-9);
        cplex.setWarning(env.getNullStream());
        IloObjective   obj;
        cplex.importModel(model, file, obj, variables, constraints);
        verbose=verbose_;
        SecondSpendSolvingCPLEX=0;
        nbIteration=0;
        nbCall=0;
        //NameToId and TempDecVar are useful if an order is given in parameter
        unordered_map<string, vector<long>> NameToId;
        IloNumVarArray TempDecVar(env);
        for (int i = 0; i < variables.getSize(); ++i) {
            model.add(IloConversion(env, variables[i], ILOFLOAT));
            variables[i].setUB(IloInfinity);
            variables[i].setLB(0);
            if(variables[i].getName()[0]=='d') {
                TempDecVar.add(variables[i]);
                if (!order.empty()){
                    NameToId[string(variables[i].getName()).substr(1, string(variables[i].getName()).size()-3)]={};
                }
            }
        }
        if (!order.empty()){
            for (int i = 0; i < TempDecVar.getSize(); ++i) {
                NameToId[string(TempDecVar[i].getName()).substr(1, string(TempDecVar[i].getName()).size()-3)].push_back(i);
            }
        }
        ub = initUb;
        nbvar=cplex.getNcols();
        assert(nbvar==variables.getSize());
        nbconstr=cplex.getNrows();
        nbnodes=0;
        bool same;
        int nbvarctr;
        string last;
        string current;
        for (int i = 0; i < nbvar; ++i) {
            Neighbor.push_back({});
            OppositeDecisionVariables.push_back({});
        }
        vector<IloNumVar> var_ctr;
        nbslackvar=0;

        //Initialize Neighbor and OppositeDecisionVariables
        for (int i = 0; i < nbconstr; ++i) {
            auto it(constraints[i].getLinearIterator());
            nbvarctr=0;
            same=true;
            var_ctr.clear();
            string id =string(it.getVar().getName()).substr(0, string(it.getVar().getName()).size()-1);
            while (it.ok()) {
                var_ctr.push_back(it.getVar());
                Neighbor[it.getVar().getId()-1].push_back(i);
                if(id!= string(it.getVar().getName()).substr(0, string(it.getVar().getName()).size()-1))
                    same= false;
                ++it;
                nbvarctr++;
            }
            if(same){
                for (int j = 0; j < (int) var_ctr.size(); ++j) {
                    for (int k = 0; k < (int) var_ctr.size(); ++k) {
                        if(j!=k){
                            OppositeDecisionVariables[var_ctr[j].getId()-1].push_back(var_ctr[k].getId()-1);
                        }
                    }
                }
            }

            //Add a slack variable if mets a >= constraint
            if(constraints[i].getUB()!=constraints[i].getLB()) {
                int bufferSize = snprintf(nullptr, 0, "s_%d", nbslackvar);
                char* name = new char[bufferSize + 1]; // +1 for the null terminator
                snprintf(name, bufferSize + 1, "s_%d", nbslackvar);
                IloNumVar slack(env, 0, IloInfinity, ILOFLOAT, name);
                model.add(slack);
                variables.add(slack);
                nbslackvar++;
                SlackvariableMap[name] = nbvar;
                nbvar++;
                Neighbor.push_back({});
                constraints[i].setLinearCoef(slack,-1);
                constraints[i].setUB(constraints[i].getLB());

            }
        }
        //Read the ordering of the variables if given one
        if(!order.empty()){
            ifstream inputFile(order);
            // Check if the file is opened successfully
            if (!inputFile.is_open()) {
                cout<< "Error opening file: " << order << endl;
            }
            else {
                string line;
                getline(inputFile, line);

                inputFile.close();

                istringstream iss(line);

                std::string number;
                vector<string> temp;
                while (iss >> number) {
                    for (int i=NameToId[number].size() -1 ;i>=0 ;i--) {
                        DecisionVariables.add(TempDecVar[NameToId[number][i]]);
                    }
                }
                assert(DecisionVariables.getSize()==TempDecVar.getSize());
            }
        }else
            DecisionVariables=TempDecVar;
    }
    /*Performs a simple branch and bound
     * varassigned[i] gives the state of variable i: -1=unassigned, 0= assigned to 0, 1=assigned to 1
     * CurrDec gives the current decision variable and a state for this variable (0 or 1)
     * deconnected[i] gives if constraint i is deconnected.
     * lb is the current lb
     * */
    void NoLearn(int depth, vector<int> varassigned, pair<long,bool> CurrDec, vector<bool> deconnected,double lb){
        if(verbose>0) {
            if(CurrDec.first!=-1)
                cout<<"Depth "<<depth<<" Nodes "<<nbnodes<<" lb: "<<lb<<" ub: "<<ub<<" Dec Var: "<<variables[CurrDec.first].getName()<<" "<<CurrDec.second<<endl;
        }
        nbnodes+=1;
        vector<long> queue;
        vector<long> VarAssignedNode;
        //Assign the current decision variables and its negation if necessary
        if(CurrDec.first!=-1) {
            queue=Neighbor[CurrDec.first];
            if (!CurrDec.second) {
                varassigned[CurrDec.first]=0;
                VarAssignedNode.push_back(CurrDec.first);
                variables[CurrDec.first].setBounds(0, 0);
            } else {
                varassigned[CurrDec.first]=1;
                for (long i : OppositeDecisionVariables[CurrDec.first]) {
                    varassigned[i]=0;
                    VarAssignedNode.push_back(i);
                    variables[i].setBounds(0, 0);
                    Union(&queue,Neighbor[i]);
                }
            }
        }
        long ctr;
        vector<vector<long>> BPremove,BPassigned;
        tuple<bool, bool, vector<long>,vector<double>> resBP;
        //Apply bound propagation
        while(!queue.empty()){
            ctr=queue[0];
            if(!deconnected[ctr]) {
                resBP = BoundPropagation(constraints[ctr], varassigned, BPassigned, BPremove);
                for (long i : get<2>(resBP)) {
                    if(varassigned[i]==0) {
                        VarAssignedNode.push_back(i);
                        variables[i].setBounds(0, 0);
                    }
                    Union(&queue,Neighbor[i]);
                }
                assert(ctr == queue[0]);
                if (get<0>(resBP))
                    deconnected[ctr] = true;
            }
            queue.erase(queue.begin());
        }


        //Solve the model
        auto start = std::chrono::high_resolution_clock::now();


        // Record the end time

        //Solve the model
        cplex.solve();
        if(nbnodes>=3) {
            nbIteration += cplex.getNiterations();
            nbCall++;
        }

        auto end = std::chrono::high_resolution_clock::now();

        // Calculate the duration
        auto duration = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
        SecondSpendSolvingCPLEX+=duration.count()/1e6;
        if (cplex.getStatus() == IloAlgorithm::Optimal) {
            double newlb = cplex.getObjValue();
            if(Myceil(newlb)<ub) {
                long nextvar = -1;
                int k=0;
                bool findfracvalue=false;
                //Find the next branching var
                while (!findfracvalue && k < DecisionVariables.getSize()) {
                    double value = cplex.getValue(DecisionVariables[k]);
                    int nearest_lower_integer = floor(value);
                    int nearest_higher_integer = Myceil(value);
                    double absdist = min(abs(value - nearest_lower_integer), abs(value - nearest_higher_integer));
                    if (absdist > 1e-6){
                        findfracvalue=true;
                    }
                    if (nextvar==-1 && (findfracvalue || (staticOrder && varassigned[DecisionVariables[k].getId() - 1]==-1)))
                        nextvar = DecisionVariables[k].getId() - 1;
                    k++;
                }

                if(!findfracvalue)
                    nextvar=-1;

                if (nextvar != -1){
                    //Develops the B&B
                    NoLearn(depth+1,varassigned, {nextvar, true},deconnected,newlb);
                    if (Myceil(newlb) < ub)
                        NoLearn(depth+1,varassigned, {nextvar, false},deconnected,newlb);
                } else {
                    //A new solution has been found
                    if (ub > Myceil(newlb)) {
                        IloNumArray variableValues(env);
                        cplex.getValues(variableValues, variables);
                        ub = Myceil(newlb);
                        if(verbose>0) {
                            cout << "---------------------------" << endl;
                            cout << "New solution of cost: " << ub << endl;
                            cout << "---------------------------" << endl;
                        }
                    }
                }
            }
        }
        for (int i = 0; i < (int) VarAssignedNode.size(); ++i) {
            variables[VarAssignedNode[i]].setBounds(0, IloInfinity);
        }
    }

    virtual void solve(){
        vector<int> varassigned(variables.getSize(),-1);
        vector<bool> deconnected(constraints.getSize(),false);
        NoLearn(0, varassigned,{-1,false},deconnected,0);
    }
    void end(){
        env.end();
    }
};

#endif //LEARNLPC___NOLEARNING_H
