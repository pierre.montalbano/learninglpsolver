//
// Created by root on 06/11/23.
//

#ifndef LEARNLPC___MEMOBOUND_H
#define LEARNLPC___MEMOBOUND_H
#include "constraint.h"



class MemoBound : public BasicBB{
public:
    double lastrhs;
    int LearnOption;
    int WarmStartOption;
    bool InfoOption;
    int BaseNbctr;
    vector<vector<double>> InfoCtr;
    string nameFile;
    MemoBound(const char *filenameFile,int verbose_,string order,int LearnOption_,int WarmStartOption_,bool InfoOption_) : BasicBB(filenameFile,verbose_,order){
        lastrhs=0;
        LearnOption=LearnOption_;
        WarmStartOption=WarmStartOption_;
        InfoOption=InfoOption_;
        BaseNbctr=constraints.getSize();
        nameFile=filenameFile;
        nameFile="info"+nameFile+".csv";
    }


    /*Add  a learning mechanism to a simple branch and bound
     * lb is the current lb
     * varassigned[i] gives the state of variable i: -1=unassigned, 0= assigned to 0, 1=assigned to 1
     * CurrDec gives the current decision variable and a state for this variable (0 or 1)
     * deconnected[i] gives if constraint i is deconnected
     * If BPRremove[i] is not empty then variable i has been removed by domain consistency and BPRremove[i] provides a reason for that
     * If BPAssign[i] is not empty then variable i has been assigned by domain consistency and BPAssign[i] provides a reason for that
     * If NCRemove[i] is not equal -1 then variable i has been removed by Node Consistency and NCRemove[i] gives it's objective value at this time.
     * obj is the current objective function
     *
     * */
    MyConstraint BBLearn(int depth, double lb, vector<int> varassigned, pair<long,bool> CurrDec, vector<bool> deconnected,vector<vector<long>> BPRremove, vector<vector<long>> BPAssign,vector<double> NCRemove,IloNumArray obj,IloNumArray LastSol,IloNumVarArray BasisVar){
        if(verbose>0) {
            if(CurrDec.first!=-1)
                cout<<"Depth "<<depth<<" Memo nodes "<<nbnodes<<" lb: "<<lb<<" ub: "<<ub<<" Dec Var: "<<variables[CurrDec.first].getName()<<" "<<CurrDec.second<<" nb added ctrs: "<<nbslackvar<<endl;
        }
        nbnodes+=1;
        vector<long> queue;
        MyConstraint resFuse(nbvar);
        vector<long> VarAssignedNode;
        //Initialize or resize (if a constraint has been learned) the different structures
        assert(cplex.getNcols()==variables.getSize());
        assert(nbvar==variables.getSize());
        MyConstraint DualProof(nbvar);
        while (obj.getSize()!=nbvar)
            obj.add(0);
        while ((int) deconnected.size()!=nbconstr)
            deconnected.push_back(false);
        while((int)varassigned.size()!=nbvar)
            varassigned.push_back(-1);
        long ctr;
        tuple<bool, bool, vector<long>,vector<double>> resBP;
        bool donttest=false;
        //Assign the current decision variables and its negation if necessary
        if(CurrDec.first!=-1) {
            queue=Neighbor[CurrDec.first];
            if (!CurrDec.second) {
                varassigned[CurrDec.first]=0;
                VarAssignedNode.push_back(CurrDec.first);
                assert(variables[CurrDec.first].getName()[0]!='s');
                variables[CurrDec.first].setBounds(0, 0);
            } else {
                varassigned[CurrDec.first]=1;
                for (long i : OppositeDecisionVariables[CurrDec.first]) {
                    varassigned[i]=0;
                    BPAssign[CurrDec.first].push_back(i);
                    VarAssignedNode.push_back(i);
                    variables[i].setBounds(0, 0);
                    assert(variables[i].getName()[0]!='s');
                    Union(&queue,Neighbor[i]);
                }
            }
        }

        //Apply domain consistency, recover if a conflict has been found and updates  BPAssign, BPRremove
        while(!queue.empty()){
            ctr=queue[0];
            if(!deconnected[ctr]) {
                resBP = BoundPropagation(constraints[ctr], varassigned, BPAssign, BPRremove);
                assert(ctr == queue[0]);
                if (get<0>(resBP))
                    deconnected[ctr] = true;
                if(get<1>(resBP)){
                    if(InfoOption && ctr>=BaseNbctr){
                        InfoCtr[ctr][7]+=1;
                    }
                    //If a conflict appeared recover an unbounded dual proof constraint
                    assert(get<3>(resBP).size()!=0);
                    for (int i = 0; i < (int)get<2>(resBP).size(); ++i) {
                        assert(variables[get<2>(resBP)[i]].getName()[0]!='s');
                        DualProof.coef[get<2>(resBP)[i]]=ub*get<3>(resBP)[i];
                        if(varassigned[get<2>(resBP)[i]]==1 &&  DualProof.coef[get<2>(resBP)[i]]>obj[get<2>(resBP)[i]])
                            DualProof.coef[get<2>(resBP)[i]]=obj[get<2>(resBP)[i]];
                    }
                    DualProof.rhs=max(ub*constraints[ctr].getUB(),(double)ub);
                    DualProof.BPFusion(BPRremove);
                    DualProof.NCFusion(NCRemove);

                    for (int i = 0; i < (int) VarAssignedNode.size(); ++i) {
                        variables[VarAssignedNode[i]].setBounds(0, IloInfinity);
                    }
                    return DualProof;
                }
                for (long i : get<2>(resBP)) {
                    if(varassigned[i]==0) {
                        if(InfoOption && ctr>=BaseNbctr){
                            InfoCtr[ctr][7]+=1;
                        }
                        VarAssignedNode.push_back(i);
                        assert(variables[i].getName()[0]!='s');
                        variables[i].setBounds(0, 0);
                    }
                    Union(&queue,Neighbor[i]);
                }
            }
            queue.erase(queue.begin());
        }

        //Modify the objective function
        IloObjective objective = cplex.getObjective();
        objective.setLinearCoefs(variables,obj);
        auto start = std::chrono::high_resolution_clock::now();
        if(LastSol.getSize()!=0 && WarmStartOption!=0){
            IloNumArray nulldualsol(env);
            for (int i = 0; i < constraints.getSize(); ++i) {
                nulldualsol.add(0);
            }
            if(WarmStartOption==1 || WarmStartOption==3)
                cplex.setStart(LastSol,NULL,BasisVar,NULL,NULL,NULL);
            if(WarmStartOption==2 || WarmStartOption==4)
                cplex.setStart(LastSol,NULL,BasisVar,NULL,nulldualsol,constraints);
        }
        //Solve the model
        cplex.solve();
        if(nbnodes>=3) {
            nbIteration += cplex.getNiterations();
            nbCall++;
        }
        auto end = std::chrono::high_resolution_clock::now();

        // Calculate the duration
        auto duration = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
        SecondSpendSolvingCPLEX+=duration.count()/1e6;
        bool Fuse=false;
        double newlb=lb;
        if (cplex.getStatus() == IloAlgorithm::Optimal) {
            IloNumArray duals(env);
            //Recover a dual proof constraint
            if(cplex.getObjValue()>0.000001) {
                newlb += cplex.getObjValue();
                cplex.getDuals(duals, constraints);
                if(InfoOption){
                    assert(constraints.getSize()>= BaseNbctr);
                    for (int i = BaseNbctr; i < constraints.getSize(); ++i) {
                        if(duals[i]>1e-6 || duals[i]<-1e-6){
                            InfoCtr[i-BaseNbctr][4]+=1;
                            InfoCtr[i-BaseNbctr][5]+=duals[i];
                            InfoCtr[i-BaseNbctr][6]+=depth;
                        }
                    }
                }
                DualProof.BuildDualProof(duals, constraints, SlackvariableMap);
                
                for (int i = 0; i < (int)DualProof.coef.size(); ++i) {
                    if(variables[i].getName()[0]!='s') {
                        if (varassigned[i] != 0) {
                            assert(DualProof.coef[i] <= obj[i] + 1e-6);
                        }
                    }
                }
                assert(DualProof.rhs < cplex.getObjValue() + 1e-6 && DualProof.rhs > cplex.getObjValue() - 1e-6);
            }
            IloNumArray reducedCosts(env);
            //Get next decision variable
            if(Myceil(newlb)<ub) {
                long nextvar = -1;
                int k=0;
                bool findfracvalue=false;
                //Get the reduced costs to reparametrize the problem
                if(cplex.getObjValue()>0.000001) {
                    cplex.getReducedCosts(reducedCosts, variables);
                    for (int i = 0; i < nbvar; ++i) {
                        assert(nbvar==varassigned.size());
                        assert(i<varassigned.size());
                        assert(varassigned[i]>=-1);
                        assert(reducedCosts[i]>=-1e-9 || varassigned[i]==0);
                        assert(obj[i]-DualProof.coef[i]<=reducedCosts[i]+1e-6 && obj[i]-DualProof.coef[i]>=reducedCosts[i]-1e-6);
                        if(reducedCosts[i]<0)
                            reducedCosts[i]=0;
                    }
                }else {
                    reducedCosts = obj;
                }
                //Performs Node Consistency
                queue.clear();
                vector<double> NewNCremove(varassigned.size(),-1);
                assert(Neighbor.size()==varassigned.size());
                assert(reducedCosts.getSize()==variables.getSize());
                vector<int> TosetBounds;
                for (int i = 0; i < (int) NewNCremove.size(); ++i) {
                    if (varassigned[i] == -1 && newlb + reducedCosts[i] > ub && variables[i].getName()[0]!='s') {
                        NewNCremove[i] = reducedCosts[i];
                        varassigned[i] = 0;
                        assert( variables[i].getName()[0]!='s');
                        TosetBounds.push_back(i);
                        //variables[i].setBounds(0, 0);
                        VarAssignedNode.push_back(i);
                        Union(&queue, Neighbor[i]);
                    }
                }
                vector<vector<long>> NewBPremove,NewBPAssign;
                for (int i = 0; i < (int)BPRremove.size(); ++i) {
                    NewBPremove.push_back({});
                    NewBPAssign.push_back({});
                }
                while(!queue.empty() ){
                    ctr=queue[0];
                    if(!deconnected[ctr]) {
                        resBP = BoundPropagation(constraints[ctr], varassigned, NewBPAssign, NewBPremove);
                        for (long i : get<2>(resBP)) {
                            if(varassigned[i]==0){
                                assert(variables[i].getName()[0]!='s');
                                //variables[i].setBounds(0, 0);
                                TosetBounds.push_back(i);
                                VarAssignedNode.push_back(i);
                            }
                            Union(&queue,Neighbor[i]);
                        }
                        assert(!get<1>(resBP));
                        assert(ctr == queue[0]);
                        if (get<0>(resBP))
                            deconnected[ctr] = true;
                    }
                    queue.erase(queue.begin());
                }
                int firstunassigned=-1;
                //Find the next branching var
                while (nextvar==-1  && k < DecisionVariables.getSize()) {
                    double value = cplex.getValue(DecisionVariables[k]);
                    int nearest_lower_integer = floor(value);
                    int nearest_higher_integer = Myceil(value);
                    double absdist = min(abs(value - nearest_lower_integer), abs(value - nearest_higher_integer));
                    //cout<<variables[DecisionVariables[k].getId() - 1]<<" "<<cplex.getValue(DecisionVariables[k])<<" "<<varassigned[DecisionVariables[k].getId() - 1]<<" "<<obj[DecisionVariables[k].getId() - 1]<<endl;
                    if (absdist > 1e-6){
                        findfracvalue=true;
                        if(varassigned[DecisionVariables[k].getId() - 1]==-1)
                            nextvar = DecisionVariables[k].getId() - 1;
                    }
                    if(varassigned[DecisionVariables[k].getId() - 1]==-1)
                        firstunassigned=DecisionVariables[k].getId() - 1;
                    if (nextvar==-1 && varassigned[DecisionVariables[k].getId() - 1]==-1 && staticOrder)
                        nextvar = DecisionVariables[k].getId() - 1;
                    k++;
                }
                IloNumVarArray BasisVar(env);
                IloCplex::BasisStatusArray bstat(env);
                cplex.getBasisStatuses(bstat,variables);
                for (int i = 0; i < nbvar; ++i) {
                    if(bstat[i]==1 || WarmStartOption<3) {
                        BasisVar.add(variables[i]);
                    }
                }
                IloNumArray NewSol(env);
                cplex.getValues(BasisVar,NewSol);
                assert(BasisVar.getSize()==NewSol.getSize());
                assert(NewSol.getSize()==BasisVar.getSize());
                for (int i = 0; i < (int) TosetBounds.size(); ++i) {
                    variables[TosetBounds[i]].setBounds(0, 0);
                }
                if(!findfracvalue)
                    nextvar=-1;
                if(findfracvalue && nextvar==-1){
                    nextvar=firstunassigned;
                    IloNumArray duals2(env);
                    //Recover a dual proof constraint

                    if(nextvar==-1) {
                        MyConstraint DualProof2(nbvar);
                        IloObjective objective = cplex.getObjective();
                        objective.setLinearCoefs(variables,reducedCosts);
                        cplex.solve();
                        newlb+=cplex.getObjValue();
                        cplex.getDuals(duals2, constraints);

                        DualProof2.BuildDualProof(duals2, constraints, SlackvariableMap);
                        DualProof2.BPFusion(NewBPremove);
                        DualProof2.NCFusion(NewNCremove);
                        for (int i = 0; i < (int)DualProof2.coef.size(); ++i) {
                            DualProof.coef[i]+=DualProof2.coef[i];
                        }
                        DualProof.rhs+=DualProof2.rhs;
                        donttest=true;
                    }
                }

                if (nextvar != -1){
                    MyConstraint ctr1= BBLearn(depth+1, newlb,varassigned, {nextvar, true},deconnected,NewBPremove,NewBPAssign,NewNCremove,reducedCosts,NewSol,BasisVar);
                    MyConstraint ctr2(nbvar);
                    if (Myceil(newlb) < ub) {
                        assert(NewSol.getSize() == BasisVar.getSize());
                        ctr2 = BBLearn(depth + 1, newlb, varassigned, {nextvar, false}, deconnected, NewBPremove,NewBPAssign, NewNCremove, reducedCosts, NewSol, BasisVar);
                    }

                    DualProof.CompleteCtr(nbvar);
                   /*cout<<"+++++++++++++++++++++++"<<endl;
                    cout<<"lb"<<" "<<lb<<" newlb "<<newlb<<" "<<ub<<" "<<ctr1.rhs<<" "<<ctr2.rhs<<" "<<DualProof.rhs<<endl;
                    cout<<"nextva "<<nextvar<<" "<<variables[nextvar].getName()<<endl;
                    if(CurrDec.first!=-1)
                        cout<<"currdec "<<CurrDec.first<<" "<<variables[CurrDec.first].getName()<< " "<<CurrDec.second<<endl;*/

                    MyConstraint resFuse(nbvar);
                    //If the two children returned a constraint then appaly MemoResolution
                    if(ctr1.rhs!= 0 && ctr2.rhs!=0) {
                        resFuse = FusionResolution(ctr1, ctr2, nextvar, OppositeDecisionVariables[nextvar]);
                        for (int i = 0; i < (int)resFuse.coef.size(); ++i) {
                            DualProof.coef[i]+=resFuse.coef[i];
                        }
                        DualProof.rhs+=resFuse.rhs;
                        if(resFuse.rhs!=0)
                            Fuse=true;

                        if(resFuse.rhs!=0 && LearnOption==2){
                            //Remove from the learned constraint the variables removed by NC and domain consistency
                            resFuse.BPFusion(BPRremove);
                            resFuse.NCFusion(NCRemove);
                            LearnConstraint(resFuse);
                            if(InfoOption)
                                InfoCtr.back()[0]=depth;
                        }
                    }
                } else {
                    // A new solution has been found
                    if (ub > Myceil(newlb)) {
                        IloNumArray variableValues(env);
                        ub = Myceil(newlb);
                        if(verbose>0) {
                            cout << "---------------------------" << endl;
                            cout << "New solution of cost: " << ub << endl;
                            cout << "---------------------------" << endl;
                        }
                    }
                }
            }
        }else{
            //If the problem is infeasible recover a farkas ray and build a dual proof constraint.
            IloNumArray duals(env);
            IloConstraintArray  Ctr(env);
            try {
                cplex.dualFarkas(Ctr,duals);
                DualProof.BuildDualProof(duals, constraints, SlackvariableMap);
                for (int i = 0; i < nbvar; ++i) {
                    DualProof.coef[i]=DualProof.coef[i]*1000;
                }
                DualProof.rhs=DualProof.rhs*1000;
                DualProof.BPFusion(BPRremove);
                DualProof.NCFusion(NCRemove);
                for (int i = 0; i < nbvar; ++i) {
                    if(varassigned[i]!=0 && obj[i]<DualProof.coef[i])
                        DualProof.coef[i]=obj[i];
                }
                assert(DualProof.rhs>0);
                for (int i = 0; i < (int) VarAssignedNode.size(); ++i) {
                    variables[VarAssignedNode[i]].setBounds(0, IloInfinity);
                }
            } catch (IloException& e) {
                cerr << "Concert Exception: " << e << endl;
            }
            return DualProof;
        }
        //Learn the constraint returned by the learning procedure
        if(DualProof.rhs!=0){
            //Remove from the learned constraint the variables removed by NC and domain consistency
            DualProof.BPFusion(BPRremove);
            DualProof.NCFusion(NCRemove);
            for (int i = 0; i < (int) DualProof.coef.size(); ++i) {
                if(variables[i].getName()[0]!='s')
                    if(varassigned[i]!=0) {
                        /*if(!donttest && DualProof.coef[i] > obj[i]+1e-8)
                            cout<<variables[i].getName()<<" "<<i<<" "<<DualProof.coef[i]<<" "<<obj[i]<<" "<<endl;*/
                        assert(donttest || DualProof.coef[i] <= obj[i]+1e-6);
                    }
            }
            if(LearnOption==1) {
                LearnConstraint(DualProof);
                if(InfoOption)
                    InfoCtr.back()[0]=depth;
            }
            if(LearnOption==3  && (lb!=newlb || Fuse)) {
                LearnConstraint(DualProof);
                if(InfoOption)
                    InfoCtr.back()[0]=depth;
            }

        }else
            DualProof.clear();
        for (int i = 0; i < (int) VarAssignedNode.size(); ++i) {
            variables[VarAssignedNode[i]].setBounds(0, IloInfinity);
        }
        return DualProof;
    }

    void LearnConstraint(MyConstraint ctr){

        double NegValue;
        NegValue=0;
        for (int i = 0; i < (int) ctr.coef.size(); ++i) {
            if(variables[i].getName()[0]!='s' && ctr.coef[i]!=0){
                if(ctr.coef[i]<0)
                    NegValue+=ctr.coef[i];

            }
        }
        ctr.CompleteCtr(nbvar);

        //add a new slack variable

        int bufferSize = snprintf(nullptr, 0, "s_%d", nbslackvar);
        char* name = new char[bufferSize + 1]; // +1 for the null terminator
        snprintf(name, bufferSize + 1, "s_%d", nbslackvar);
        IloNumVar slack(env, 0, IloInfinity, ILOFLOAT, name);
        IloObjective obj = cplex.getObjective();
        obj.setLinearCoef(slack,0);

        //Create the new constraint and add it to the model
        IloExpr lhs(env);
        assert(nbvar==(int) ctr.coef.size());
        int nbvarctr=0;
        for (int i = 0; i < nbvar; ++i) {
            if(variables[i].getName()[0]!='s') {
                if (min(ctr.coef[i], ctr.rhs) > 0 + 1e-7) {
                    lhs += min(ctr.coef[i], ctr.rhs) * variables[i];
                    nbvarctr+=1;
                    Neighbor[i].push_back(nbconstr);
                }
            }else if(ctr.coef[i]>0 + 1e-7) {
                lhs += ctr.coef[i] * variables[i];
                nbvarctr+=1;
            }
        }
        lhs+= -1 * slack;
        IloRange customConstraint(env, ctr.rhs, lhs, ctr.rhs,"l");
        model.add(slack);
        variables.add(slack);
        Neighbor.push_back({nbconstr});
        model.add(customConstraint);
        if(InfoOption) {
            vector<double> info(8,0);
            info[1]=nbnodes;
            info[2]=nbvarctr;
            info[3]=ctr.rhs;
            InfoCtr.push_back(info);
        }

        // Assign custom identifiers to variables and add them to the map
        SlackvariableMap[name] = nbvar;
        nbvar++;
        constraints.add(customConstraint);
        nbconstr++;
        nbslackvar++;
        lastrhs=ctr.rhs;
        //cplex.extract(model);
        delete[] name;
    }
    void solve(){
        IloObjective obj=cplex.getObjective();
        auto it =obj.getLinearIterator();
        IloNumArray objarray(env);
        IloNumArray Initsol(env);
        IloNumVarArray Initbasis(env);
        vector<vector<long>> BPempty;

        for (int i = 0; i < variables.getSize(); ++i) {
            objarray.add(0);
            BPempty.push_back({});
        }
        while(it.ok()){
            objarray[it.getVar().getId()-1]=it.getCoef();
            ++it;
        }
        vector<double> empty(variables.getSize(),-1);
        vector<int> varassigned(variables.getSize(),-1);
        vector<bool> deconnected(constraints.getSize(),false);
        BBLearn(0, 0,varassigned,{-1,false},deconnected,BPempty,BPempty,empty,objarray,Initsol,Initbasis);
        if(InfoOption){
            cout<<nameFile<<endl;
            std::ofstream file(nameFile);
            std::ofstream file2("info.csv", std::ios::app);
            if (!file.is_open()) {
                std::cerr << "Error: Unable to open file " << "info.csv"<< std::endl;
                return;
            }

            // Write header
            file << "Depth,Node,Size,rhs,nbdual!=0,valdual,depth useful,val removed," << std::endl;

            // Write data
            for (const auto& row : InfoCtr) {
                for (size_t i = 0; i < row.size(); ++i) {
                    file << row[i];
                    file2 << row[i];

                    if (i < row.size() - 1) {
                        file << ",";
                        file2 << ",";
                    }
                }
                file << std::endl;
                file2 << std::endl;
            }
            file.close();
            file2.close();
            //cplex.exportModel("end.lp");
        }
    }
};


#endif //LEARNLPC___MEMOBOUND_H
