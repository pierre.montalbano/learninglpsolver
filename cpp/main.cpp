
static long initUb = 1000000;
static bool staticOrder=false;
#include <getopt.h>
#include "NoLearning.h"
#include "MemoBound.h"
using namespace std;
/* NoLearning.h defines a basic branch and bound.
 * MemoBound.h adds to a learning mechanism to the basic B&B
 * constraint.h provides different tools to propagate, modify and create te constraints.
 * main.cpp intialize and calls the solvers.
 *
 */

int main(int argc, char *argv[]) {
    // Initialize variables to store option values
    int verboseOption = 0;
    string orderOption;
    int LearnOption=1;
    bool NoLearnOption=true;
    int WarmStartOption=0;
    bool InfoOption=false;
    string pythonScript(argv[0]);
    pythonScript.erase(pythonScript.size()-9);
    pythonScript+= "wcsp2lp-support_learncpp.py";
    // Process command-line options
    int opt;
    while ((opt = getopt(argc, argv, "v:o:n:l:u:s:w:i:")) != -1) {
        switch (opt) {
            case  'n':
                if(stoi(optarg)==0)
                    NoLearnOption= false;
                else
                    NoLearnOption= true;
                break;
            case  'l':
                LearnOption=stoi(optarg);
                break;
            case 'o':
                orderOption = optarg;
                break;
            case 'v':
                verboseOption = stoi(optarg);
                break;
            case 'u':
                initUb = stol(optarg);
                break;
            case 'w':
                WarmStartOption =  stoi(optarg);
                break;
            case 'i':
                if(stoi(optarg)==0)
                    InfoOption= false;
                else
                    InfoOption= true;
                break;
            case 's':
                if(stoi(optarg)==0)
                    staticOrder = false;
                else
                    staticOrder = true;
                break;
            default:
                // Handle unknown or invalid options
                cerr << "Usage: " << "./memobound" << " filename " << " [-v1] for verbosity"<<endl<<"[-o oderfile] to indicate an ordering"<<endl<<" [-n1] (-n0) to enable or disable the search with a basic B&B" <<endl<<"[-l0] to disable the search with a learning approach [-l1] to learn constraints after the sum, [-l2] to learn them before the sum "<<endl<<"[-w1] to enable warm start using the last primal solution, [-w2] using 0 dual solution, [-w3] using last primal and 0 dual solution"<<endl<<" [-i] to get information on the constraints in info.txt";
                return 1;
        }
    }

    string inputFile = argv[optind];
    if(inputFile[inputFile.size()-2]=='s'){
        string command = "python2 " + pythonScript + " " + inputFile+ " "+inputFile.substr(0, inputFile.size()-4)+"lp";
        system(command.c_str());
        inputFile=inputFile.substr(0, inputFile.size()-4)+"lp";
    }else
        assert(inputFile[inputFile.size()-2]=='l');
    //Initialization
    MemoBound Memosolver(inputFile.c_str(), verboseOption, orderOption,LearnOption,WarmStartOption,InfoOption);
    BasicBB Basicsolver(inputFile.c_str(), verboseOption, orderOption);
    long ubbasic;
    long nodebasic;
    double TimeSolveLPBasic;
    double AverageIte=0;
    double durationNolearn=0;
    //Basic B&B
    if(NoLearnOption) {
        auto start = std::chrono::high_resolution_clock::now();
        Basicsolver.solve();
        auto end = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
        durationNolearn+=duration.count()/1e6;
        ubbasic=Basicsolver.ub;
        nodebasic=Basicsolver.nbnodes;
        TimeSolveLPBasic=Basicsolver.SecondSpendSolvingCPLEX;
        if(Basicsolver.nbCall!=0)
            AverageIte=Basicsolver.nbIteration/Basicsolver.nbCall;
        Basicsolver.end();
    }
    long Learnub;
    long Learnnode;
    double LastRhs;
    long nbaddedctr;
    double TimeSolveLPBLearn;
    double LearnAverageIte=0;
    double durationLearn=0;
    //B&B + Learning
    if(LearnOption>0) {
        MemoBound Memosolver(inputFile.c_str(), verboseOption, orderOption,LearnOption,WarmStartOption,InfoOption);
        auto start = std::chrono::high_resolution_clock::now();
        Memosolver.solve();
        auto end = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
        durationLearn+=duration.count()/1e6;
        Learnub=Memosolver.ub;
        Learnnode=Memosolver.nbnodes;
        LastRhs=Memosolver.lastrhs;
        nbaddedctr=Memosolver.nbslackvar;
        TimeSolveLPBLearn=Memosolver.SecondSpendSolvingCPLEX;
        if(Memosolver.nbCall!=0)
            LearnAverageIte=Memosolver.nbIteration/Memosolver.nbCall;
        Memosolver.end();
    }
    if(NoLearnOption)
        cout<<"No learning optimal solution is "<<ubbasic<<" in "<<nodebasic<<" nodes, "<<TimeSolveLPBasic<<" spent solving LPs with "<< AverageIte<< " iterations in average and full time is: "<<durationNolearn<<endl;

    if(LearnOption>0)
        cout<<"Learning optimal solution is "<<Learnub<<" in "<<Learnnode<<" nodes"<<", last rhs is "<<LastRhs<<" and nb of added constraints is "<< nbaddedctr<<" and "<<TimeSolveLPBLearn<< " spent solving LPs with "<<LearnAverageIte<< " iterations in average and full time is: "<<durationLearn<<endl;
    if(LearnOption>0 && NoLearnOption){

        assert(ubbasic==Learnub);
        assert(nodebasic>=Learnnode);
        assert(Myceil(LastRhs)==Learnub || Learnnode==0 );
    }

    return 0;
}
