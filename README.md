# MemoBound

MemoBound is an exact MILP solver. It develops a branch and bound algorithm and relies on CPLEX to solve the linear relaxation at each node. MemoBound recovers the optimal dual solution and the reduced costs to reparametrize the problem and learn memo constraints ( unpublished work). 

This project is still under development and is not robust or well-optimized. 

We recommend using the C++ version which is more user-friendly than the Python version. MemoBound reads a wcsp file and uses the script wcsp2lp-support_learncpp.py to transform it into an LP file. 


#Compilation and usage

To compile the C++ version, the user needs to replace "/path/to/cplex/" with the path to its cplex installation. Then type "make" (or "make debug" for the debugging version) in the cpp directory.

Usage: ./memobound filename.wcsp [-v1] for verbosity
[-o orderfile] to indicate an ordering
[-n1] (-n0) to enable or disable the search with a basic B&B
[-l1] (-l0) to enable or disable the search with a learning approach


#Contact

pierre.montalbano@gmail.com
